package com.thoughtworks.vapasi;

public class RomanConverter {

    public Integer convertRomanToArabicNumber(String roman) {

        int number = 0;

        switch (roman) {

            case "I":
                number = 1;
                break;
            case "II":
                number = 2;
                break;
            case "III":
                number = 3;
                break;
            case "IV":
                number = 4;
                break;

            case "V":
                number = 5;
                break;
            case "VI":
                number = 6;
                break;
            case "VII":
                number = 7;
                break;
            case "IX":
                number = 9;
                break;
            case "X":
                number = 10;
                break;


        }

        return Integer.valueOf(number);

    }
}




